from matplotlib import pyplot as plt
import numpy as np
import matplotlib.cm as cm
import time



class Car():
	
	def __init__(self, x=0, dx=5, bumper = None):
		self.x = x
		self.bumper = bumper # keeps track of the car in front of it 
		self.dx = dx

	def move(self):
		self.x = self.x + self.dx 
	
	def change_dx(self, dx):
		self.dx = dx 

	def speedUp(self):
		self.dx = 5 # car starts travelling at the normal rate of traffic again 
                
	def seeDeer(self, slow, deerSeen): 
		if np.random.randint(0,10) > 1 and deerSeen == False: # probability of a car seeing a deer 
			self.dx = 1
		


CarNumber = 100 # total number of cars 
car_list = [] # an array of all the cars
car_pos = [] # position of all the cars x position for plotting 
graph_y = [] # position of all the cars y positions for plotting 
xDeer = [] # x position of a deer seen for plotting 
yDeer = [] # y position of the deer for plotting 

for i in range(0,CarNumber): # initialising 100 cars 
	if i == 0:
                
                # the first car starts at x = 0, with dx = 5 and is following no one
		new_car = Car(0, 5, None)  
		car_list.append(new_car)
	else:
                # all the cars behind the first car, each following the car ahead of it  
		new_car = Car(car_list[i-1].x - 10, car_list[i-1].dx, car_list[i-1]) 
		car_list.append(new_car)

        # adding each car's x position to the array 
	car_pos.append(new_car.x)
        
        # plotting everyone on the y = 20 line 
	graph_y.append(20)

# all cars initially don't see a deer 
deerSeen = False

# keeping track of the time 
t0 = time.time()

# running a loop till the last car leaves the highway 
while car_list[len(car_list) -1].x < 100 :

	print ("pos of last car: ", car_list[len(car_list) -1].x)

        # the arrays reset so the plots keep changing (for animation purposes) 
	del car_pos[:]
	del graph_y[:]
	del xDeer [:]
	del yDeer [:]
	#carselect = 3

        # makes the cars return to their normal speed after a few steps 
        if car_list[len(car_list) -1].x % 5 == 0:
                deerSeen = False

        # for each of the 1000 cars...  
	for i in range(0,CarNumber):

                # for each car in a certain x position
		if car_list[len(car_list) -1].x % 10 == 0:

                        # a random car gets selected 
		
			carselect = np.random.randint(0,(CarNumber - 1))

                        # makes sure the car that was selected is in our highway zone 
                        
			while car_list[carselect].x > 100 and car_list[carselect].x < 0 :
				carselect = np.random.randint(0,(CarNumber - 1))

                        # if the car is inside the highway zone that we can see 
                
			if car_list[carselect].x < 100 and car_list[carselect].x > 0 :

                                # makes the selected car see the deer 

				if i == carselect:
					car_list[i].seeDeer(1,deerSeen)
					deerSeen = True
					xDeer.append(car_list[carselect].x)
					yDeer.append(20)
					print("Hi deer")
					#print carselect

                                # makes all the cars behind carselect slow down too 
                                
			        if i > carselect:
					car_list[i].change_dx(car_list[i-1].dx)

                        # if the car has not seen the deer, goes back to normal speed of traffic 

			if deerSeen == False:
					car_list[i].speedUp()

		car_list[i].move()
		car_pos.append(car_list[i].x)
		graph_y.append(20)


       #plots 

	colours = cm.rainbow(np.linspace(0,1,len(graph_y)))
	plt.figure(1)
	plt.title("Highway to Deer Simulation")
	plt.scatter(car_pos,graph_y,color = colours)
	plt.plot(xDeer, yDeer, 'r+', mew=10, ms=20)
	plt.axis([0, 100, 10, 30])
	plt.draw()
	plt.pause(.0001)
	plt.clf()

	plt.figure(2)
 	plt.title("Deer Seen Counter")
	plt.plot(xDeer, yDeer, 'ro')

	#plt.figure(3)
	#plt.plot(xDeer, yDeer, time) 
	
 
# total time taken for the simulation

t1 = time.time()	
print("time elapsed", t1-t0)

